//
//  autoTestProvingGroundTests.swift
//  autoTestProvingGroundTests
//
//  Created by Kerwin Lumpkins on 3/8/17.
//  Copyright © 2017 Kerwin Lumpkins. All rights reserved.
//

import XCTest
@testable import autoTestProvingGround

class autoTestProvingGroundTests: XCTestCase {
    var fvc:FirstViewController!
    
    override func setUp() {
        super.setUp()
        
        let storyboard = UIStoryboard (name: "Main", bundle: nil)
        fvc = storyboard.instantiateInitialViewController() as! FirstViewController
        
        
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testUnit_NormalInputInt() {
        let hoo = try! fvc.convertInputToNum(inStr: "3")
        XCTAssertEqual(hoo, 3.0)
    }
    
    func testUnit_NormalInputFloat() {
        let hoo = try! fvc.convertInputToNum(inStr: "3.2")
        XCTAssertEqual(hoo, 3.2)
    }
    
    func testUnit_NegInputNotNumber () {
        if let _ = try? fvc.convertInputToNum(inStr: "Kerwin") {
            XCTFail("No error was thrown for bad input")
        }
        
    }
    
    func testUnit_NegInputIsEmpty () {
        if let _ = try? fvc.convertInputToNum(inStr: "") {
            XCTFail("No error was thrown for bad input")
        }
        
    }
        
        
        
}
