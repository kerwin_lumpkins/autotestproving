//
//  acidDefinitions.swift
//  autoTestProvingGround
//
//  Created by Kerwin Lumpkins on 5/23/17.
//  Copyright © 2017 Kerwin Lumpkins. All rights reserved.
//

import XCTest

let app = XCUIApplication()

enum elname :String {
    case acidBtnAdd2
    case acidTextFieldInput
    case acidTextFieldOutput
    case acidLabelCando

}

let acid:[elname:XCUIElement] = [
    elname.acidBtnAdd2           : app.buttons["acid.first.add2Button"],
    elname.acidTextFieldInput    : app.textFields["acid.textfield.First.InputForAdd2"],
    elname.acidTextFieldOutput   : app.textFields["acid.field.outputForAdd2"]
]






