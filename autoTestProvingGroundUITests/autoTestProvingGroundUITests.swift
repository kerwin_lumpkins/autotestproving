//
//  autoTestProvingGroundUITests.swift
//  autoTestProvingGroundUITests
//
//  Created by Kerwin Lumpkins on 3/8/17.
//  Copyright © 2017 Kerwin Lumpkins. All rights reserved.
//

import XCTest

class autoTestProvingGroundUITests: XCTestCase {
        
    override func setUp() {
        super.setUp()
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false
        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testUI_NormalEnterInt() {
        //
        let app = XCUIApplication()
        let inField = app.textFields["acid.field.InputForAdd2"]
        inField.tap()
        inField.typeText("3")
        app.buttons["acid.button.add2"].tap()
        
        let boo = app.textFields["acid.field.outputForAdd2"].value as! String
        
        XCTAssertEqual(boo, "5.0")
        
    }
    
    func testUI_NormalEnterFloat() {
        
        let app = XCUIApplication()
        let inField = app.textFields["acid.field.InputForAdd2"]
        inField.tap()
        inField.typeText("3.2")
        app.buttons["acid.button.add2"].tap()
        
        let boo = app.textFields["acid.field.outputForAdd2"].value as! String
        
        XCTAssertEqual(boo, "5.2")
        
    }
    
    func testUI_NegNoNumberInput () {
        let app = XCUIApplication()
                
        app.buttons["acid.button.add2"].tap()
        
        let boo = app.textFields["acid.field.InputForAdd2"].value as! String
        
        XCTAssertEqual(boo, "Nothing entered")
    }
    
    func testUI_NegNonNumberInput () {
        let app = XCUIApplication()
        let inField = app.textFields["acid.field.InputForAdd2"]
        inField.tap()
        inField.typeText("Leroy Jenkins")
        app.buttons["acid.button.add2"].tap()
        
        let boo = app.textFields["acid.field.InputForAdd2"].value as! String
        
        XCTAssertEqual(boo, "Ain't a num")
    }
    
    
    
    
}
