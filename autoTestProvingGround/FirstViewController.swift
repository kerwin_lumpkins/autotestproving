//
//  FirstViewController.swift
//  autoTestProvingGround
//
//  Created by Kerwin Lumpkins on 3/8/17.
//  Copyright © 2017 Kerwin Lumpkins. All rights reserved.
//

import UIKit

class FirstViewController: UIViewController {

    @IBOutlet weak var InputField: UITextField!
    
    @IBOutlet weak var OutputField: UITextField!
    
    @IBOutlet weak var buttonAdd2: UIButton!
    
    enum inputError:Error {
        case inputWasEmpty
        case inputWasNotANumber
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func Add2(_ sender: Any) {
        do{
            let add2result = try convertInputToNum(inStr: InputField.text!)  + 2
                OutputField.text = "\(add2result)"
        } catch inputError.inputWasEmpty{
            InputField.text = "Nothing entered"
        }
        catch inputError.inputWasNotANumber{
            InputField.text = "Ain't a num"
        }
        catch {}
        
        
    }
    
    func convertInputToNum (inStr:String) throws ->Float {
        guard !(inStr.isEmpty) else {throw inputError.inputWasEmpty}
        guard let result = Float(inStr) else {throw inputError.inputWasNotANumber}
        return result
    }

}

